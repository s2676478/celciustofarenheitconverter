package com.example.celciustofarenheit3.converter;

public class Temperature {
    public double getTemperature (String temperature) {
        return Double.parseDouble(temperature) * 1.8 + 32;
    }

}
